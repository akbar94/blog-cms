<?php

function insertCategory()
{
    if (isset($_POST['category_title'])) {
        global $connection;
        $category_title = $_POST['category_title'];
        if ($category_title == "" || empty($category_title)) {
            echo "this field should not be empty";
        } else {
            $sql = "INSERT INTO categories(title) VALUES('" . $category_title . "') ";
            if (mysqli_query($connection, $sql)) {
                echo "category added successfully!";
            } else {
                echo "Error " . $sql . "<br>" . mysqli_error($connection);
            }
        }
    }
}

function allCategories()
{
    global $connection;
    $categories = mysqli_query($connection, "SELECT * FROM categories");
    while ($rows = mysqli_fetch_assoc($categories)) {
        ?>

        <tr>
            <td><?php echo $rows['id'] ?></td>
            <td><?php echo $rows['title'] ?></td>
            <td><a class="btn btn-danger"
                   href="categories.php?delete=<?php echo $rows['id'] ?>">Delete</a> <a
                        class="btn btn-success"
                        href="categories.php?edit=<?php echo $rows['id'] ?>">Edit</a></td>
        </tr>


        <?php
    }
    if (isset($_GET['delete'])) {
        $id = $_GET['delete'];
        $delete_query = mysqli_query($connection, "DELETE FROM categories WHERE id = '$id' ");
        header("Location: categories.php");

    }
}

//----------------------------------------------posts functions---------------------------------

function allPosts()
{
    global $connection;
    $post_query = mysqli_query($connection, "SELECT * FROM posts");
    $posts = array();
    while ($row = mysqli_fetch_assoc($post_query)) {
        $posts[] = $row;
    }
    return $posts;
}

function create_post(){
    
}