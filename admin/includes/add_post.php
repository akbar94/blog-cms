<?
if (isset($_POST['create_post'])){
    create_post();
}
?>

<div class="col-xs-6 center-block">
    <form action="" method="post" enctype="multipart/form-data">

        <div class="form-group">
            <label for="title">post title</label>
            <input type="text" class="form-control" name="title">
        </div>
        <div class="form-group">
            <label for="title">post category id</label>
            <input type="text" class="form-control" name="category_id">
        </div>
        <div class="form-group">
            <label for="title">tags</label>
            <input type="text" class="form-control" name="tags">
        </div>
        <div class="form-group">
            <label for="title">image</label>
            <input type="file" class="form-control-file" name="image">
        </div>
        <div class="form-group">
            <label for="title">date</label>
            <input type="date" class="form-control" name="date">
        </div>
        <div class="form-group">
            <label for="title">content</label>
            <textarea type="text" rows="10" class="form-control" name="content"></textarea>
        </div>
        <div class="form-group">
            <label for="checkbox"> publish </label>
            <input type="checkbox" class="checkbox-inline">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-info btn-lg" value="Add Post" name="create_post">
        </div>


    </form>
</div>
