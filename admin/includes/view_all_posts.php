<table class="table table-hover table-condensed table-responsive table-bordered">
    <thead>
    <tr class="active">
        <th>Id</th>
        <th>category id</th>
        <th>content</th>
        <th>author</th>
        <th>tags</th>
        <th>comment count</th>
        <th>status</th>
        <th>date</th>
        <th>image</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php
    //------------------ALL CATEGORIES SECTION -------------------------------------------------
    foreach (allPosts() as $post) {
        $image_path = $post['image_path'];
        echo "<tr>";
        echo "<td>" . $post['id'] . "</td>";
        echo "<td>" . $post['title'] . "</td>";
        echo "<td>" . $post['author'] . "</td>";
        echo "<td>" . $post['content'] . "</td>";
        echo "<td>" . $post['tags'] . "</td>";
        echo "<td>" . $post['comment_count'] . "</td>";
        echo "<td>" . $post['status'] . "</td>";
        echo "<td>" . $post['date'] . "</td>";
        echo "<td><img class='img-responsive' width='100' src='../images/$image_path' alt=''></td>";
        echo "<td>" ."<a href='posts.php?delete=".$post['id']."' class='btn btn-danger'>Delete</a> <a href='posts.php?edit=".$post['id']."' class='btn btn-success'>Edit</a>" . "</td>";
        echo "</tr>";
    }
    ?>

    </tr>
    </tbody>
</table>